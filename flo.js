var flo = require('fb-flo'),
    path = require('path'),
    fs = require('fs');

var server = flo( './js',
    {
    port: 8888,
    host: 'localhost',
    verbose: true,
    glob: [ '*.js', '*.scss' ]
  }, resolver);

server.once('ready', function() {
  console.log('Ready!');
});

function resolver(filepath, callback) {

  console.log("-----------------");
  console.log(filepath);
  console.log(path.extname(filepath));
  console.log("-----------------");

  callback({
    resourceURL: 'js/bundle' + path.extname(filepath)
    ,contents: fs.readFileSync('./js/' + filepath)
    ,reload: false
  });
}