'use strict';

var gulp = require('gulp');
var postcss = require('gulp-postcss');
var sass = require('gulp-ruby-sass');
var autoprefixer = require('autoprefixer-core');
var csswring = require('csswring');
var sourcemaps = require('gulp-sourcemaps');
var csslint = require('gulp-csslint');
var csscomb = require('gulp-csscomb');
var filter = require('gulp-filter');
var plumber = require('gulp-plumber');
var gutil = require('gulp-util');
var watchify = require('watchify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');


gulp.task('css', generateCSS);
gulp.task('css-live', ['css'], liveCSS);
gulp.task('js-live', liveJS);
gulp.task('live', ['css-live', 'js-live']);

function liveCSS () {
    gulp.watch(['src/**/*.scss'], ['css']);
}

function generateCSS () {
    var sassOptions = {
        style: 'expanded',
        sourcemapPath: './scss',
        require: 'sass-globbing',
    };

    var autoprefixerOptions = {
        browsers: ['last 10 Chrome versions', 'last 10 Firefox versions', 'ie >= 8', 'Safari >= 5.1'],
    };

    var processors = [
        autoprefixer(autoprefixerOptions),
        // csswring()
    ];

    var cssFilter = filter(['*.css']);

    return gulp.src('src/**/*.scss')
        .pipe(plumber())
        .pipe(csscomb())
        .pipe(gulp.dest('src'))
        .pipe(sass(sassOptions))
        .pipe(sourcemaps.init({loadMaps: true}))
            .pipe(postcss(processors))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('css'))
        .pipe(cssFilter)
        .pipe(csslint('csslintrc.json'))
        .pipe(csslint.reporter())
    ;
}

function liveJS () {
    var bundler = watchify(browserify('./src/index.js', watchify.args));

    bundler.on('update', function (files) {
        gutil.log("changed files: ", files);
        rebundle();
    });

    bundler.on('log', gutil.log);

    return rebundle();

    function rebundle () {
        return bundler.bundle()
            .on('error', gutil.log.bind(gutil, 'Browserify Error:'))
            .pipe(source('bundle.js'))
            .pipe(gulp.dest('js'))
        ;
    }
}